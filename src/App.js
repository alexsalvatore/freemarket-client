import react, { useState } from "react";
import "./App.css";
import PostingView from "./components/PostingView";
import BoardView from "./components/BoardView";
import IDView from "./components/IDView";

function App() {
  const [profileEdition, setProfileEdition] = useState(true);

  const handleProfileEdition = (value) => {
    setProfileEdition(value);
  };

  return (
    <div>
      <div className="title">
        <h1>Freemarket😇 V.0.1</h1>
        <h4>The free market of ideas - アイデアの自由市場</h4>
        <div>
          Made by{" "}
          <a href="https://twitter.com/alexkrunch" target="_blank">
            Ælx Krunch
          </a>
        </div>
      </div>
      {profileEdition && (
        <IDView handleProfileEdition={handleProfileEdition}></IDView>
      )}
      {!profileEdition && (
        <PostingView handleProfileEdition={handleProfileEdition}></PostingView>
      )}
      {/*<PendingView></PendingView>*/}
      <BoardView></BoardView>
    </div>
  );
}

export default App;
