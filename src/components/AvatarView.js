import React, { useEffect, useState } from "react";
import ChainService from "../services/ChainService";
import Constants from "../utils/Constants";
import hashAvatar from "hash-avatar";
import sha256 from "crypto-js/sha256";

const AvatarView = (props) => {
  const [avatar, setAvatar] = useState("");
  const [defaultAvatar, setDefault] = useState("");
  const [hash, setHash] = useState("");

  const updateAvatar = () => {
    const hashed = sha256("").toString();
    setHash(hashed);
    setDefault(hashAvatar(hashed, { size: Constants.sizeAvatar }));

    if (!props.avatarHash) return;
    const chainService = ChainService.getInstance();
    const newAvatar = chainService.findAvatarByHash(props.avatarHash);
    setAvatar(newAvatar);
  };

  useEffect(() => {
    const chainService = ChainService.getInstance();
    chainService.subrcribeToNewChain((blocks) => {
      updateAvatar();
    });
    updateAvatar();
  }, [props.avatarHash, props.publicKey]);

  return (
    <div>
      {avatar && (
        <img
          className="avatar-img"
          width={Constants.sizeAvatar + "px"}
          height={Constants.sizeAvatar + "px"}
          src={avatar}
        ></img>
      )}

      {!avatar && defaultAvatar && (
        <div
          className="avatar-img"
          dangerouslySetInnerHTML={{
            __html: defaultAvatar,
          }}
        ></div>
      )}
    </div>
  );
};

export default AvatarView;
