import React, { useState, useEffect } from "react";
import ChainService from "../services/ChainService";
import PostView from "./PostView";

const BoardView = (props) => {
  const [blocks, setBlocks] = useState([]);
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    const chainService = ChainService.getInstance();
    chainService.subrcribeToNewChain((newBlocks) => {
      let blocksCopy = [...newBlocks];
      blocksCopy = blocksCopy.reverse();
      setBlocks(blocksCopy);
    });
    chainService.subrcribeToCountdown((count) => {
      setSeconds(count);
    });
    chainService.fecthChain();
  }, []);

  return (
    <div>
      {/*<div className="view-centered ">
        <b>{seconds} seconds</b> before auto-update
      </div>*/}
      {blocks &&
        blocks.map((block) => {
          return (
            <div key={block.hash} className="view-block">
              {block.contents &&
                block.contents.map((content, index) => {
                  return (
                    <PostView
                      key={index}
                      content={content}
                      hash={block.hash}
                    ></PostView>
                  );
                })}
              <div className="post-content post-info">
                <b>
                  Block n°{block.height}, at{" "}
                  {new Date(Number(block.timestamp)).toString()} block hash: #
                  {block.hash}, nonce: {block.nonce}
                </b>
                , publisher: #{block.publisher.slice(0, 66) + "..."}, parent
                hash: #{block.parentHash}
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default BoardView;
