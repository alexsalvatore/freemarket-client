import React, { useEffect, useState } from "react";
import PostContent from "../models/PostContent";
import Block from "../models/Block";
import ChainService from "../services/ChainService";
import IDService from "../services/IDService";
import { Button, ProgressBar } from "react-bootstrap";
import AvatarView from "./AvatarView";

const PostingView = (props) => {
  const [postContent, setPost] = useState({ title: "", message: "" });
  const [hash, setHash] = useState("");
  const [lastBlock, setlastBlock] = useState({});
  const [profile, setProfile] = useState({});
  const [difficulty, setDifficulty] = useState(0);
  // const [minedBlock, setMinedBlock] = useState({});
  const [isProcessing, setProcessing] = useState(false);

  useEffect(() => {
    const chainService = ChainService.getInstance();
    chainService.subrcribeToNewChain((newBlocks) => {
      const profileNew = IDService.getInstance().getProfile();
      setProfile(profileNew);
      const newLastBLock = chainService.getLastBlock();
      setDifficulty(
        chainService.getDifficultyForKey(
          profileNew.publicKey,
          newLastBLock.height + 1
        )
      );
      setlastBlock(chainService.getLastBlock());
    });
    const profileNew = IDService.getInstance().getProfile();
    setProfile(profileNew);
    const newLastBLock = chainService.getLastBlock();
    setDifficulty(
      chainService.getDifficultyForKey(
        profileNew.publicKey,
        newLastBLock && newLastBLock.height ? newLastBLock.height + 1 : 0
      )
    );
    setlastBlock(newLastBLock);
  }, []);

  const launchProcess = (mine) => {
    return (e) => {
      e.preventDefault();
      setProcessing(true);
      const idService = IDService.getInstance();
      const chainService = ChainService.getInstance();
      const profile = idService.getProfile();

      const avatarExist = chainService.findAvatarByHash(profile.avatarHash);
      const contentSigned = new PostContent({
        ...postContent,
        idName: profile.name,
        idDescription: profile.description,
        idSite: profile.site,
        idBTC: profile.btc,
        idETH: profile.eth,
        idPublicKey: profile.publicKey,
        idAvatar: !avatarExist ? profile.avatar : "",
        idAvatarHash: profile.avatarHash,
      });
      contentSigned.sign();
      if (mine) {
        const block = new Block({
          height: lastBlock ? lastBlock.height + 1 : 0,
          parentHash: lastBlock ? lastBlock.hash : "",
          contents: [contentSigned],
          publisher: profile.publicKey,
        });

        setTimeout(() => {
          block.proofOfWork(() => {
            console.log("Mined block", block);
            setProcessing(false);
            setHash(block.hash);
            const chainService = ChainService.getInstance();
            chainService.postBlock(block).then(() => {
              setPost({ title: "", message: "" });
            });
          });
        }, 3000);
      } else {
        chainService.postPendingContent(contentSigned).then(() => {
          setPost({ title: "", message: "" });
          setProcessing(false);
        });
      }
    };
  };

  const onContentChange = (e) => {
    e.preventDefault();
    setPost({ ...postContent, [e.target.name]: e.target.value });
  };

  const launchIDEdit = (e) => {
    if (props.handleProfileEdition) props.handleProfileEdition(true);
  };

  return (
    <div className={"view-content posting"}>
      <div>
        {profile.publicKey && (
          <div className="post-profile">
            {profile.avatarHash && (
              <div>
                <AvatarView
                  avatarHash={profile.avatarHash}
                  publicKey={profile.publicKey}
                ></AvatarView>
              </div>
            )}
            <div>
              <div>
                {" "}
                <b>
                  {profile.name + "#" + profile.publicKey.slice(0, 5)}
                  {ChainService.getInstance().isTrusted(profile.publicKey) && (
                    <span> ✅</span>
                  )}
                </b>
              </div>
              <div>
                <Button variant="outline-primary" onClick={launchIDEdit}>
                  ✏️ Edit
                </Button>
              </div>
            </div>
          </div>
        )}
      </div>

      <div className="posting-content">
        <div>
          title:{" "}
          <input
            name="title"
            type="text"
            onChange={onContentChange}
            value={postContent.title}
            disabled={isProcessing}
          ></input>
        </div>

        <div>
          message:
          <br />
          <textarea
            name="message"
            rows="4"
            cols="50"
            value={postContent.message}
            onChange={onContentChange}
            disabled={isProcessing}
          ></textarea>
        </div>
        <div>
          <Button
            variant={"primary"}
            onClick={launchProcess(true)}
            disabled={isProcessing || !postContent.message}
          >
            ⛏️ Mine!
          </Button>
          {isProcessing && (
            <div className={"view-centered"}>
              <div>⛏️ Mining is running...</div>
              <ProgressBar animated now={100} />
            </div>
          )}
          {lastBlock && (
            <div>
              <div className="post-info">
                Future height: {lastBlock.height + 1}, Parent hash:{" "}
                {lastBlock.hash}
              </div>
              <div>
                <ProgressBar style={{ marginBottom: "14px" }}>
                  <ProgressBar
                    label={"difficulty: " + difficulty}
                    variant={
                      (difficulty < 4 && "success") ||
                      (difficulty < 6 && "warning") ||
                      "danger"
                    }
                    now={difficulty * 10}
                    key={1}
                  />
                </ProgressBar>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default PostingView;
