import React from "react";
import AvatarView from "./AvatarView";
import TextView from "./TextView";
import ChainService from "../services/ChainService";
import { Button } from "react-bootstrap";

const PostView = (props) => {
  return (
    <div className={"post-content"}>
      {props.content.title && (
        <h2 className="post-Title">{props.content.title}</h2>
      )}
      {!props.content.title && (
        <h2 className="post-Title">
          <i>Untitled</i>
        </h2>
      )}
      <h5>#{props.hash.slice(0, 25)}...</h5>
      <div>
        {props.content.idPublicKey && (
          <div className="post-profile">
            {props.content.idAvatarHash && (
              <div>
                <AvatarView
                  avatarHash={props.content.idAvatarHash}
                  publicKey={props.content.idPublicKey}
                ></AvatarView>
              </div>
            )}
            <div>
              <b>
                {props.content.idName +
                  "#" +
                  props.content.idPublicKey.slice(0, 5)}
                {ChainService.getInstance().isTrusted(
                  props.content.idPublicKey
                ) && <span> ✅</span>}
              </b>
            </div>
            <div>
              {props.content.idDescription && props.content.idDescription}
            </div>
            <div>
              {props.content.idSite && (
                <a href={props.content.idSite} target="_blank">
                  🔗{props.content.idSite.slice(0, 12)}...
                </a>
              )}
            </div>
            <div>
              {props.content.idBTC && (
                <span>
                  💴 BTC: <b>{props.content.idBTC}</b>
                </span>
              )}
            </div>

            <div>
              {props.content.idETH && (
                <span>
                  💴 ETH: <b>{props.content.idETH}</b>
                </span>
              )}
            </div>
          </div>
        )}
      </div>

      <div className={"post-text"}>
        <div>
          <TextView text={props.content.message}></TextView>
        </div>
      </div>
    </div>
  );
};

export default PostView;
