import React, { useEffect, useState } from "react";
import UTXOPoolService from "../services/UTXOPoolService";

const EarningsView = (props) => {
  const [earning, setEarning] = useState(0);

  useEffect(() => {
    const poolService = UTXOPoolService.getInstance();
    setEarning(poolService.getOrCreateKey(props.publicKey));
    poolService.subrcribeToNewPool(() => {
      setEarning(poolService.getOrCreateKey(props.publicKey));
    });
  }, [props.publicKey]);

  return (
    <div>
      Own <i className="price-info">{earning} $</i>
    </div>
  );
};

export default EarningsView;
