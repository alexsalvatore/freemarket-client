import React, { useEffect, useState } from "react";
import ChainService from "../services/ChainService";
import Block from "../models/Block";
import IDService from "../services/IDService";
import PostContent from "../models/PostContent";

const PendingView = (props) => {
  const [pendings, setPendings] = useState([]);
  const [lastBlock, setlastBlock] = useState({});
  const [hash, setHash] = useState("");
  const [isProcessing, setProcessing] = useState(false);

  useEffect(() => {
    const chainService = ChainService.getInstance();
    chainService.subrcribeToNewPending((pendings) => {
      setPendings([...pendings]);
    });
    chainService.fetchPending();
    chainService.subrcribeToNewChain((newBlocks) => {
      setlastBlock(chainService.getLastBlock());
    });
    setlastBlock(chainService.getLastBlock());
  }, []);

  const launchMining = () => {
    setProcessing(true);
    const idService = IDService.getInstance();
    const profile = idService.getProfile();

    const contentPendings = [];
    pendings.forEach((pending) => {
      contentPendings.push(new PostContent(pending));
    });

    const block = new Block({
      height: lastBlock ? lastBlock.height + 1 : 0,
      parentHash: lastBlock ? lastBlock.hash : "",
      contents: contentPendings,
      publisher: profile.publicKey,
    });

    setTimeout(() => {
      block.proofOfWork(() => {
        console.log("Mined block", block);
        setProcessing(false);
        setHash(block.hash);
        const chainService = ChainService.getInstance();
        chainService.postBlock(block).then(() => {
          setProcessing(false);
          const chainService = ChainService.getInstance();
          chainService.fetchPending();
        });
      });
    }, 3000);
  };

  return (
    <div className={"view-content posting"}>
      {lastBlock && (
        <div className="post-info">
          New height: {lastBlock.height + 1} Parent hash: {lastBlock.hash}
        </div>
      )}
      <div>{pendings.length} are pending.</div>
      <div>
        <button onClick={launchMining} disabled={isProcessing}>
          ⛏️ Mine!
        </button>
        {isProcessing && <i> ⛏️ Mining is running...</i>}
        {!isProcessing && hash && <i>{hash}</i>}
      </div>
    </div>
  );
};

export default PendingView;
