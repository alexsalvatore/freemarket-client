import React, { useState, useEffect } from "react";
import Linkify from "react-linkify";

const TextView = (props) => {
  const [lines, setLines] = useState([]);
  const [medias, setMedias] = useState([]);

  const geturl = new RegExp(
    "(^|[ \t\r\n])((ftp|http|https|gopher|mailto|news|nntp|telnet|wais|file|prospero|aim|webcal):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))",
    "g"
  );

  useEffect(() => {
    if (!props.text) return;

    const newLines = props.text.split("\n");
    setLines(newLines);

    const urlsParsed = props.text.match(geturl);
    if (!urlsParsed) return;
    const urlsTmp = [];
    urlsParsed.forEach((url) => {
      if (
        url.indexOf("jpg") >= 0 ||
        url.indexOf("png") >= 0 ||
        url.indexOf("gif") >= 0 ||
        url.indexOf("webp") >= 0
      ) {
        urlsTmp.push(url);
      }
    });
    setMedias(urlsTmp);
  }, [props.text]);

  return (
    <div>
      {lines &&
        lines.map((line, index) => {
          return (
            <Linkify key={index}>
              {line}
              <br />
            </Linkify>
          );
        })}
      {medias &&
        medias.map((url, index) => (
          <img key={index} src={url} width="450px"></img>
        ))}
    </div>
  );
};

export default TextView;
