import React, { useEffect, useState } from "react";
import { generatePair } from "../utils/Crypto";
import IDService from "../services/IDService";
import AvatarView from "./AvatarView";
import UTXOPoolService from "../services/UTXOPoolService";
import EarningsView from "./EarningsView";
import { Button } from "react-bootstrap";

const IDView = (props) => {
  const [profile, setProfile] = useState({
    name: "",
    description: "",
    site: "",
    btc: "",
    eth: "",
    avatar: "",
    privateKey: "",
    publicKey: "",
  });

  useEffect(() => {
    const poolService = UTXOPoolService.getInstance();
    const idService = IDService.getInstance();

    idService.load().then((tmpProfile) => {
      setProfile({ ...profile, ...tmpProfile });
    });

    poolService.subrcribeToNewPool((pool) => {
      console.log(pool, profile.publicKey);
    });
  }, []);

  const saveProfile = (newProfile) => {
    const idService = IDService.getInstance();
    idService.setProfile(newProfile);
  };

  const generateKeys = (e) => {
    e.preventDefault();
    let newKeys = generatePair();
    const newProfile = {
      ...profile,
      ...newKeys,
    };
    setProfile(newProfile);
    saveProfile(newProfile);
  };

  const onIdChange = (e) => {
    e.preventDefault();
    const newProfile = { ...profile, [e.target.name]: e.target.value };
    setProfile(newProfile);
    saveProfile(newProfile);
  };

  const onAvatarChange = (avatar) => {
    const newProfile = { ...profile, avatar };
    setProfile(newProfile);
    saveProfile(newProfile);
  };

  const stopIDEdit = (e) => {
    if (props.handleProfileEdition) props.handleProfileEdition(false);
  };

  return (
    <div className={"view-content posting"}>
      {
        <AvatarView
          avatarHash={profile.avatarHash}
          publicKey={profile.publicKey}
        ></AvatarView> /*
        <div
          dangerouslySetInnerHTML={{
            __html: profile.avatarHash,
          }}
        ></div>*/
      }
      {/*<AvatarEditor
        dataurl={profile.avatar}
        onAvatarChange={onAvatarChange}
      ></AvatarEditor>*/}
      {/*<EarningsView publicKey={profile.publicKey}></EarningsView>*/}
      <div>
        Name:{" "}
        <input
          name="name"
          type="text"
          value={profile.name}
          onChange={onIdChange}
        ></input>
      </div>
      {
        <div>
          Description: <br />
          <textarea
            name="description"
            rows="4"
            cols="50"
            value={profile.description}
            onChange={onIdChange}
          ></textarea>
        </div>
      }
      <div>
        Site:{" "}
        <input
          name="site"
          type="url"
          value={profile.site}
          onChange={onIdChange}
        ></input>
      </div>
      <div>
        BTC donation:{" "}
        <input
          name="btc"
          type="text"
          value={profile.btc}
          onChange={onIdChange}
        ></input>
      </div>
      <div>
        ETH donation:{" "}
        <input
          name="eth"
          type="text"
          value={profile.eth}
          onChange={onIdChange}
        ></input>
      </div>
      <div>
        Public key:{" "}
        <input
          name="publicKey"
          type="text"
          value={profile.publicKey}
          onChange={onIdChange}
        ></input>
        ⚠️
      </div>
      <div>
        Private key:{" "}
        <input
          name="privateKey"
          type="text"
          value={profile.privateKey}
          onChange={onIdChange}
        ></input>
        ⚠️
      </div>
      <div className={"view-buttons"}>
        <Button variant="outline-primary" onClick={generateKeys}>
          🎲 Regen key pair
        </Button>
        <Button variant="primary" onClick={stopIDEdit}>
          Go to posting!
        </Button>
      </div>
    </div>
  );
};
export default IDView;
