import React, { useEffect, useState, useRef } from "react";
import Constants from "../utils/Constants";
import sha256 from "crypto-js/sha256";

const AvatarEditor = (props) => {
  const [linkURL, setLinkURl] = useState("");
  const [avatarHash, setAvatarHash] = useState("");
  const [size, setSize] = useState(0);
  let refCanvas = useRef();
  let refInput = useRef();

  useEffect(() => {
    if (!props.dataurl) {
      let canvas = refCanvas.current;
      let ctx = canvas.getContext("2d");
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      refInput.current.value = "";
      setLinkURl("");
      return;
    }
    const img = new Image();
    img.crossOrigin = "anonymous";
    img.onload = () => {
      let canvas = refCanvas.current;
      let ctx = canvas.getContext("2d");
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, 0, 0, Constants.sizeAvatar, Constants.sizeAvatar);
    };
    img.src = props.dataurl;
    setSize(props.dataurl.length / 1000);
    setAvatarHash(sha256(props.dataurl).toString());
  }, [props.dataurl]);

  const onFileSelected = (e) => {
    var URL = window.URL;
    var url = URL.createObjectURL(e.target.files[0]);
    setLinkURl("");
    renderCanvas(url);
  };

  const onURLSelected = (e) => {
    refInput.current.value = "";
    renderCanvas(e.target.value);
  };
  const renderCanvas = (url) => {
    const img = new Image();
    img.crossOrigin = "anonymous";
    img.onload = () => {
      let canvas = refCanvas.current;
      let ctx = canvas.getContext("2d");

      let x = 0;
      let y = 0;
      let scale = 1;
      let width = img.width;
      let height = img.height;
      if (width > height) {
        scale = width / height;
        width = Constants.sizeAvatar * scale;
        height = Constants.sizeAvatar;
        x = (width - Constants.sizeAvatar) * -0.5;
      } else {
        scale = height / width;
        height = Constants.sizeAvatar * scale;
        width = Constants.sizeAvatar;
        y = (height - Constants.sizeAvatar) * -0.5;
      }
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, x, y, width, height);
      console.log(canvas);
      if (props.onAvatarChange) props.onAvatarChange(canvas.toDataURL());
    };
    img.src = url;
  };

  return (
    <div>
      <canvas
        name="canvasImage"
        ref={refCanvas}
        width={Constants.sizeAvatar + "px"}
        height={Constants.sizeAvatar + "px"}
      ></canvas>
      {avatarHash && <div>hash: {avatarHash}</div>}
      {size && <div>size: {size} Ko</div>}
      <br />
      <input ref={refInput} type="file" onChange={onFileSelected}></input>{" "}
      <br />
      Or select an URL <br />
      <input type="url" onChange={onURLSelected}></input>
    </div>
  );
};

export default AvatarEditor;
