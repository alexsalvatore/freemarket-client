import elliptic from "elliptic";
const ec = new elliptic.ec("secp256k1");

export function generatePair() {
  const keypair = ec.genKeyPair();
  window.keypair = keypair;
  return {
    publicKey: keypair.getPublic("hex"),
    privateKey: keypair.getPrivate("hex"),
  };
}

export function sign(message, privateKey, publicKey) {
  try {
    console.log("#############################");
    console.log(message, privateKey, publicKey);
    const keypair = ec.keyFromPrivate(privateKey, "hex");
    const signature = keypair.sign(message).toDER("hex");
    const result = verifySignature(message, signature, publicKey);

    console.log("sign() result", result);

    return signature;
  } catch (error) {
    return "invalid signature";
  }
}

export function verifySignature(message, signature, publicKey) {
  try {
    const keypair = ec.keyFromPublic(publicKey, "hex");
    return ec.verify(message, signature, keypair);
  } catch (error) {
    return false;
  }
}

function ConvertStringToHex(str) {
  var arr = [];
  for (var i = 0; i < str.length; i++) {
    arr[i] = ("00" + str.charCodeAt(i).toString(16)).slice(-4);
  }
  return "\\u" + arr.join("\\u");
}
