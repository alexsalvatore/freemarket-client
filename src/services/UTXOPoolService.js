import { Subject } from "rxjs";
import Block from "../models/Block";
import PostContent from "../models/PostContent";

export default class UTXOPoolService {
  pool = {};
  subjectNewPool = new Subject();
  static PRICE_BLOCK_MINING = 12.5;
  static PRICE_FREEPOSTING = 1.5;
  static PRICE_100B = 10.5;

  static getInstance() {
    if (!this._instance) {
      this._instance = new UTXOPoolService();
    }
    return this._instance;
  }

  subrcribeToNewPool(subscription) {
    this.subjectNewPool.subscribe(subscription);
  }

  updatePool() {
    console.log(this.pool);
    this.subjectNewPool.next(this.pool);
  }

  clearPool() {
    this.pool = [];
  }

  addBlock(block) {
    //for block miner
    const minerPayload = this.getOrCreateKey(block.publisher);
    this.pool[block.publisher] = minerPayload + this.getBlockReward(block);
    //Fee for each content publisher
    block.contents.forEach((data) => {
      const content = new PostContent(data);
      if (content.idPublicKey !== block.publisher) {
        const posterPayload = this.getOrCreateKey(content.idPublicKey);

        this.pool[content.idPublicKey] = posterPayload - this.getPostCost(data);

        this.pool[block.publisher] = minerPayload + this.getPostCost(data);
      }
    });
  }

  getPostCost(postData) {
    const content = new PostContent(postData);
    /*const postPrice =
      (content.size / 200) * UTXOPoolService.PRICE_100B +
      UTXOPoolService.PRICE_FREEPOSTING;*/
    const postPrice = UTXOPoolService.PRICE_FREEPOSTING;
    return postPrice;
  }

  getBlockReward(blockData) {
    const block = new Block(blockData);
    let reward = UTXOPoolService.PRICE_BLOCK_MINING;
    let totalKO = 0;

    block.contents.forEach((data) => {
      const content = new PostContent(data);
      //if (content.idPublicKey !== block.publisher) {
      //totalKO += content.size;
      reward += UTXOPoolService.PRICE_FREEPOSTING;
      //}
    });
    //reward += (totalKO / 100) * UTXOPoolService.PRICE_100B;
    return reward;
  }

  getOrCreateKey(key) {
    if (!this.pool[key]) this.pool[key] = 0;
    return this.pool[key];
  }
}
