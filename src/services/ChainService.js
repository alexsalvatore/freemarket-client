import { Subject } from "rxjs";
import Block from "../models/Block";
import { maxBy, reduce, unfold, reverse, values, prop } from "ramda";
//import UTXOPoolService from "./UTXOPoolService";

export default class ChainService {
  subjectNewChain = new Subject();
  countDownActive = false;
  countDown = 0;
  static COUNT_MAX = 30;
  timer;
  subjectCountdown = new Subject();

  constructor() {
    this.blocks = [];
    //this.timer = setInterval(() => this.updateCountdown(), 1000);
  }

  static getInstance() {
    if (!this._instance) {
      this._instance = new ChainService();
    }
    return this._instance;
  }

  subrcribeToNewChain(subscription) {
    this.subjectNewChain.subscribe(subscription);
  }

  subrcribeToCountdown(subscription) {
    this.subjectCountdown.subscribe(subscription);
  }

  updateCountdown() {
    /*
    if (this.countDownActive) {
      if (this.countDown >= 0) {
        this.countDown -= 1;
        this.subjectCountdown.next(this.countDown);
      }
      if (this.countDown === 0) {
        this.countDown = ChainService.COUNT_MAX;
        this.fecthChain();
      }
    }*/
  }

  updateChain() {
    /*const utxPoolService = UTXOPoolService.getInstance();
    utxPoolService.clearPool();
    this.blocks.forEach((block) => {
      utxPoolService.addBlock(block);
    });
    utxPoolService.updatePool();*/
    this.blocks = [...this.longestChain(this.blocks)];
    this.subjectNewChain.next(this.blocks);
  }
  /*
  fetchPending() {
    const formData = new FormData();
    formData.append("action", "getPending");
    fetch("http://localhost:3000/freemarket/", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("result pending", data.result);
        this.pendings = data.result;
        this.updatePending();
      });
  }*/

  fecthChain() {
    this.countDownActive = false;
    const formData = new FormData();
    formData.append("action", "getFeed");
    fetch("http://localhost:3000/freemarket/", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        //this.blocks = [];
        //let blocksResult = this.longestChain(data.result);
        let blocksResult = data.result;
        blocksResult.sort((a, b) => {
          if (a.height < b.height) {
            return -1;
          }
          if (a.height > b.height) {
            return 1;
          }
          return 0;
        });


        //Block
        blocksResult.forEach((block) => {
          const blockObj = new Block(block);
          const found = this.blocks.find((b) => b.hash === blockObj.hash);
          if (!found && blockObj.isValid()) this.blocks.push(blockObj);
        });

        this.updateChain();

        this.countDownActive = true;
        this.countDown = ChainService.COUNT_MAX;
        this.updateCountdown();
      })
      .catch((err) => {
        console.log("Error fetching blocks", err);

        this.countDownActive = true;
        this.countDown = ChainService.COUNT_MAX;
        this.updateCountdown();
      });
  }

  getDifficultyForKey(publicKey, height = 0) {
    let numBlock48h = 0;

    let isIssueInChain = false; // Do we have a complete cycle of block chain with a genesis block?
    let totalBlockPublished = 0;

    let heightExist = false;
    this.blocks.forEach((block) => {
      //Just to know if it's a future block we are testing
      if (Number(block.height) === height) heightExist = true;
      if (block.height === 0 || block.edito) isIssueInChain = true;
      if (block.publisher === publicKey && Number(block.height) < height) {
        totalBlockPublished += 1;
        const blockDate = new Date(Number(block.timestamp));
        const millis = Date.now() - blockDate.getTime();
        if (millis < 48 * 60 * 60 * 1000) {
          numBlock48h += 1;
        }
      }
    });

    if (height > 15)
      console.log("getDifficultyForKey()", isIssueInChain, totalBlockPublished);

    if (isIssueInChain && totalBlockPublished === 0) return 5;
    if (!heightExist) numBlock48h += 1;
    return Math.round(numBlock48h);
    //return 3;
  }

  isTrusted(publicKey) {
    let numBlock = 0;
    this.blocks.forEach((block) => {
      if (block.publisher === publicKey) numBlock += 1;
    });
    return numBlock >= 9;
  }

  /*
  postPendingContent(content) {
    const formData = new FormData();
    formData.append("action", "addPending");
    for (var key of Object.keys(content)) {
      //console.log(key + " -> " + content[key])
      formData.append(key, content[key]);
    }
    return fetch("http://localhost:3000/freemarket/", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        return this.fetchPending();
      });
  }*/

  postBlock(block) {
    const formData = new FormData();
    formData.append("action", "addBlock");
    formData.append("contents", JSON.stringify(block.contents));
    formData.append("publisher", block.publisher);
    formData.append("nonce", block.nonce);
    formData.append("timestamp", block.timestamp);
    formData.append("parentHash", block.parentHash);
    formData.append("height", block.height);

    return fetch("http://localhost:3000/freemarket/", {
      method: "POST",
      body: formData,
    })
      .then((response) => response.json())
      .then((data) => {
        return this.fecthChain();
      });
  }

  getLastBlock() {
    const blocks = values(this.blocks);
    const maxByHeight = maxBy(prop("height"));
    const maxHeightBlock = reduce(maxByHeight, blocks[0], blocks);
    return maxHeightBlock;
  }

  getBlockByHash(hash) {
    const blocksDict = {}; // = values(this.blocks);
    this.blocks.forEach((block) => (blocksDict[block.hash] = block));
    return blocksDict[hash];
  }

  longestChain(blocksToSort) {
    const blocksDict = {}; // = values(this.blocks);
    blocksToSort.forEach((block) => (blocksDict[block.hash] = block));
    const getParent = (x) => {
      if (x === undefined) {
        return false;
      }
      return [x, blocksDict[x.parentHash]];
    };

    return reverse(unfold(getParent, this.getLastBlock()));
  }

  findAvatarByHash(hash) {
    const blockAvatar = this.blocks.find((block) => {
      if (!block.contents) return false;
      let isAvatarFound = false;
      block.contents.forEach((content) => {
        if (content.idAvatarHash === hash && content.idAvatar)
          isAvatarFound = true;
      });
      return isAvatarFound;
    });

    if (!blockAvatar) return;
    let idAvatar;
    blockAvatar.contents.forEach((content) => {
      if (content.idAvatarHash === hash && content.idAvatar) {
        idAvatar = content.idAvatar;
      }
    });
    return idAvatar;
  }
}
