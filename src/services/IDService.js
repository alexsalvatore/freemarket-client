import { Subject } from "rxjs";
import { sign } from "../utils/Crypto";
import localforage from "localforage";
import Profile from "../models/Profile";

/**
 * Returned the ID of the connected user
 */
export default class IDService {
  subjectNewProfile = new Subject();

  constructor() {
    this.profile = new Profile({
      name: "",
      description: "",
      site: "",
      eth: "",
      btc: "",
      avatar: "",
      avatarHash: "",
      privateKey: "",
      publicKey: "",
    });
  }

  static getInstance() {
    if (!this._instance) {
      this._instance = new IDService();
    }
    return this._instance;
  }

  setProfile(data) {
    this.profile = new Profile(data);
    this.save();
  }

  getProfile() {
    return new Profile(this.profile);
  }

  save() {
    return localforage.setItem("profile", JSON.stringify(this.profile));
  }

  load() {
    return localforage.getItem("profile").then((profileString) => {
      let profileMem = JSON.parse(profileString);
      if (!profileMem) return this.profile;
      this.setProfile({ ...this.profile, ...profileMem });
      return this.profile;
    });
  }

  //sign string
  sign(hash) {
    if (!this.profile || !this.profile.privateKey)
      return console.warn("No private key to sign content");

    return sign(hash, this.profile.privateKey, this.profile.publicKey);
  }
}
