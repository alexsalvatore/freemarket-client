import IDService from "../services/IDService";
import sha256 from "crypto-js/sha256";

export default class PostContent {
  constructor(content) {
    this.timestamp = content.timestamp ? content.timestamp : Date.now();
    this.channel = content.channel ? content.channel : "";
    this.title = content.title ? content.title : "";
    this.reply = content.reply ? content.reply : "";
    this.message = content.message ? content.message : "";
    // this.dataURL = content.dataURL ? content.dataURL : "";
    this.idName = content.idName ? content.idName : "";
    this.idDescription = content.idDescription ? content.idDescription : "";
    this.idSite = content.idSite ? content.idSite : "";
    this.idBTC = content.idBTC ? content.idBTC : "";
    this.idETH = content.idETH ? content.idETH : "";
    this.idPublicKey = content.idPublicKey ? content.idPublicKey : "";
    //this.idAvatar = content.idAvatar ? content.idAvatar : "";
    this.idAvatar = "";
    this.idAvatarHash = content.idAvatarHash
      ? content.idAvatarHash
      : sha256(this.idAvatar).toString();
    this.signature = content.signature ? content.signature : "";
    this._calculateHash();
    this._calculateSize();
  }

  stringify() {
    return (
      this.timestamp +
      this.channel +
      this.title +
      this.reply +
      this.message +
      // this.dataURL +
      this.idName +
      this.idDescription +
      this.idSite +
      this.idBTC +
      this.idETH +
      this.idPublicKey +
      this.idAvatar +
      this.idAvatarHash
    );
  }

  _calculateHash() {
    this.hash = sha256(this.stringify()).toString();
    return this.hash;
  }

  _calculateSize() {
    this.size = Math.round(this.stringify().length);
  }

  sign() {
    const idService = IDService.getInstance();
    this.signature = idService.sign(this.hash);
    return this;
  }
}
