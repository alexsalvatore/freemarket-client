import SHA256 from "crypto-js/sha256";

export default class Profile {
  constructor(profile) {
    if (!profile) return;
    this.name = profile.name ? profile.name : "";

    this.description = profile.description ? profile.description : "";
    this.site = profile.site ? profile.site : "";
    this.btc = profile.btc ? profile.btc : "";
    this.eth = profile.eth ? profile.eth : "";

    this.publicKey = profile.publicKey ? profile.publicKey : "";
    this.privateKey = profile.privateKey ? profile.privateKey : "";
    this.avatar = profile.avatar ? profile.avatar : "";
    this.avatarHash = SHA256(this.avatar).toString();
  }

  stringifyProfile() {
    return this.name + "." + this.publicKey;
  }

  stringifyProfileSecret() {
    return this.name + "." + this.publicKey + "." + this.privateKey;
  }

  //parseProfile (){}
}
