import sha256 from "crypto-js/sha256";
import PostContent from "./PostContent";
import ChainService from "../services/ChainService";

export default class Block {
  difficulty = 3;

  constructor(value) {
    this.height = Number(value.height);
    this.parentHash = value.parentHash;
    this.nonce = value.nonce ? value.nonce : 0;
    this.timestamp = value.timestamp ? Number(value.timestamp) : Date.now();
    this.publisher = value.publisher ? value.publisher : "";
    this.difficulty = ChainService.getInstance().getDifficultyForKey(
      this.publisher,
      this.height
    );
    this.contents = value.contents ? value.contents : [];
    this._calculateHash();
  }

  proofOfWork(callback) {
    while (!this._testHashDifficulty()) {
      this.nonce++;
      if (this.nonce % 256 === 0) {
        console.log("nonce:", this.nonce);
      }
      this.hash = this._calculateHash();
    }
    if (callback) {
      const res = this._testHashDifficulty() ? this.hash : null;
      callback(res);
    }
  }

  isValid() {
    const parent = ChainService.getInstance().getBlockByHash(this.parentHash);
    return (
      this._testHashDifficulty() &&
      (!parent || parent.height === this.height - 1)
    );
  }

  _testHashDifficulty() {
    return (
      this.hash.substring(0, this.difficulty) ===
      Array(this.difficulty + 1).join("0")
    );
  }

  _calculateHash() {
    let contentsString = "";
    this.contents.forEach((data) => {
      const content = new PostContent(data);
      contentsString += content.stringify();
    });
    this.hash = sha256(
      this.height +
        this.parentHash +
        this.nonce +
        this.timestamp +
        contentsString +
        this.publisher
    ).toString();
    return this.hash;
  }
}
